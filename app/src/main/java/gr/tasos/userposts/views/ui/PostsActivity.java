package gr.tasos.userposts.views.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import gr.tasos.userposts.R;
import gr.tasos.userposts.databinding.ActivityPostsBinding;
import gr.tasos.userposts.service.model.Post;
import gr.tasos.userposts.viewmodel.PostsViewModel;
import gr.tasos.userposts.views.adapter.PostsAdapter;

public class PostsActivity extends AppCompatActivity {
    private static final String TAG = "PostsActivity";

    private PostsViewModel postsViewModel;
    private ActivityPostsBinding binding;
    private PostsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_posts);
        postsViewModel = ViewModelProviders.of(this).get(PostsViewModel.class);

        bindViews();
        subscribeObservers();
        getIncomingIntent();
    }


    private void bindViews() {
        adapter = new PostsAdapter();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        binding.setIsLoading(postsViewModel.isPostsCalled());
    }

    private void subscribeObservers() {
        postsViewModel.getPosts().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(List<Post> posts) {
                if(posts.size() > 0) {
                    if(posts.get(0).getUserId() == postsViewModel.getUserId()) {
                        binding.setIsLoading(!postsViewModel.isPostsCalled());
                        adapter.setPosts(posts);
                    }
                }
            }
        });
    }

    private void getIncomingIntent() {
        if(getIntent().hasExtra("userId")) {
            binding.setIsLoading(!postsViewModel.isPostsCalled());
            postsViewModel.makePostsPerUser(String.valueOf(getIntent().getIntExtra("userId", 1)));
        }
    }
}
