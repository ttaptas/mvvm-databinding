package gr.tasos.userposts.views.callbacks;

import gr.tasos.userposts.service.model.User;

public interface UserClickCallback {
    void onUserClick(int userId);
}
