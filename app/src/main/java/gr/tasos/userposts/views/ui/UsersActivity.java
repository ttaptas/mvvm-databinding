package gr.tasos.userposts.views.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import gr.tasos.userposts.R;
import gr.tasos.userposts.databinding.ActivityUsersBinding;
import gr.tasos.userposts.service.model.User;

import gr.tasos.userposts.viewmodel.UsersViewModel;
import gr.tasos.userposts.views.adapter.UsersAdapter;
import gr.tasos.userposts.views.callbacks.GetUsersClickCallback;
import gr.tasos.userposts.views.callbacks.UserClickCallback;

public class UsersActivity extends AppCompatActivity implements
        UserClickCallback,
        GetUsersClickCallback {
    private static final String TAG = "UsersActivity";

    private UsersViewModel usersViewModel;

    private ActivityUsersBinding binding;
    private UsersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_users);
        usersViewModel = ViewModelProviders.of(this).get(UsersViewModel.class);

        bindViews();
        subscribeObservers();
    }

    private void bindViews() {
        binding.setIsLoading(usersViewModel.isUsersCalled());
        binding.setBtnVisible(!usersViewModel.isUsersCalled());

        binding.setCallback(this);
        adapter = new UsersAdapter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
    }

    private void subscribeObservers() {
        usersViewModel.getUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                binding.setIsLoading(!usersViewModel.isUsersCalled());
                adapter.setUsers(users);
            }
        });

    }

    @Override
    public void onUserClick(int userId) {
        Intent intent = new Intent(this, PostsActivity.class);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onGetUsersClicked() {
        binding.setIsLoading(!usersViewModel.isUsersCalled());
        binding.setBtnVisible(usersViewModel.isUsersCalled());
        usersViewModel.makeUsersCall();
    }
}
