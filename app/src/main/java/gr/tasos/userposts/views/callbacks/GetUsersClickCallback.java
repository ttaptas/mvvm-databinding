package gr.tasos.userposts.views.callbacks;

public interface GetUsersClickCallback {
    void onGetUsersClicked();
}
