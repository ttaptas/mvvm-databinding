package gr.tasos.userposts.views.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gr.tasos.userposts.R;
import gr.tasos.userposts.databinding.ListItemUserBinding;
import gr.tasos.userposts.service.model.User;
import gr.tasos.userposts.views.callbacks.UserClickCallback;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private List<User> users;
    private UserClickCallback userClickCallback;

    public UsersAdapter(UserClickCallback callback) {
        this.userClickCallback = callback;
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemUserBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_user, parent, false);
        binding.setCallback(userClickCallback);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setUser(users.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return users == null ? 0 :users.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final ListItemUserBinding binding;

        private ViewHolder(ListItemUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
