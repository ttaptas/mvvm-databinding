package gr.tasos.userposts.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gr.tasos.userposts.R;
import gr.tasos.userposts.databinding.ListItemPostBinding;
import gr.tasos.userposts.service.model.Post;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {
    private List<Post> posts;

    public PostsAdapter() {
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemPostBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_post, parent, false);
        return new PostsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PostsAdapter.ViewHolder holder, int position) {
        holder.binding.setPost(posts.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return posts == null ? 0 : posts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final ListItemPostBinding binding;

        private ViewHolder(ListItemPostBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
