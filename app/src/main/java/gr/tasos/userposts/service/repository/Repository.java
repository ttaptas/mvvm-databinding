package gr.tasos.userposts.service.repository;

import androidx.lifecycle.LiveData;

import java.util.List;

import gr.tasos.userposts.service.model.Post;
import gr.tasos.userposts.service.model.User;

public class Repository {
    private ApiClient apiClient;
    private static Repository instance;


    public synchronized static Repository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }

    private Repository() {
        apiClient = ApiClient.getInstance();
    }

    public LiveData<List<User>> getUsers() {
        return apiClient.getUsers();
    }

    public LiveData<List<Post>> getPosts() {
        return apiClient.getUserPosts();
    }

    public void makeUsersCall() {
        apiClient.makeUsersCall();
    }

    public void makePostsPerUserCall(String usedId) {
        apiClient.makePostsPerUserCall(usedId);
    }
}
