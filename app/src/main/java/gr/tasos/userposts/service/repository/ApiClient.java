package gr.tasos.userposts.service.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import gr.tasos.userposts.service.ServiceGenerator;
import gr.tasos.userposts.service.model.Post;
import gr.tasos.userposts.service.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiClient {
    private static final String TAG = "ApiClient";
    private static ApiClient instance;
    private MutableLiveData<List<User>> mUsers;
    private MutableLiveData<List<Post>> mUserPosts;

    static ApiClient getInstance(){
        if(instance == null) {
            instance = new ApiClient();
        }

        return instance;
    }

    private ApiClient() {
        mUsers = new MutableLiveData<>();
        mUserPosts = new MutableLiveData<>();
    }


    LiveData<List<User>> getUsers(){
        return mUsers;
    }

    LiveData<List<Post>> getUserPosts() {
        return mUserPosts;
    }

    void makeUsersCall() {
        ServiceGenerator.getApi().getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                mUsers.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                mUsers.setValue(null);
            }
        });
    }

    void makePostsPerUserCall(String userId) {
        ServiceGenerator.getApi().getPostPerUser(userId).enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.e(TAG, "onResponse: POSTS CALLED!!!!!");
                mUserPosts.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                mUserPosts.setValue(null);
            }
        });
    }

}
