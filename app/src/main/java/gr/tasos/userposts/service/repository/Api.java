package gr.tasos.userposts.service.repository;

import java.util.List;

import gr.tasos.userposts.service.model.Post;
import gr.tasos.userposts.service.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("users")
    Call<List<User>> getUsers();

    @GET("posts")
    Call<List<Post>> getPostPerUser(@Query("userId") String userId);
}
