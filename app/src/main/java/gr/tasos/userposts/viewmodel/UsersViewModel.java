package gr.tasos.userposts.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import gr.tasos.userposts.service.model.User;
import gr.tasos.userposts.service.repository.Repository;

public class UsersViewModel extends ViewModel {

    private Repository repository;
    private boolean usersCalled;

    public UsersViewModel() {
        super();
        repository = Repository.getInstance();
        usersCalled = false;
    }

    public LiveData<List<User>> getUsers() {
        return repository.getUsers();
    }

    public void makeUsersCall() {
        repository.makeUsersCall();
        usersCalled = true;
    }

    public boolean isUsersCalled() {
        return usersCalled;
    }

}
