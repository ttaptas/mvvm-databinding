package gr.tasos.userposts.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import gr.tasos.userposts.service.model.Post;
import gr.tasos.userposts.service.repository.Repository;

public class PostsViewModel extends ViewModel {
    private Repository repository;
    private boolean postsCalled;
    private String userId;

    public PostsViewModel() {
        super();
        repository = Repository.getInstance();
        postsCalled = false;
    }

    public LiveData<List<Post>> getPosts() {
        return repository.getPosts();
    }

    public void makePostsPerUser(String userId) {
        this.userId = userId;
        repository.makePostsPerUserCall(userId);
        postsCalled = true;
    }

    public boolean isPostsCalled() {
        return postsCalled;
    }

    public int getUserId() {
        return Integer.valueOf(userId);
    }
}
